import { ProductoService } from './../productos/producto.service';
import { ActivatedRoute } from '@angular/router';
import { Producto } from './../productos/producto';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  producto: Producto = new Producto();
  today: any = moment().format();

  constructor( private productoService: ProductoService,
               private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  guardarProducto( form: NgForm ) {
    console.log(this.today);

    if ( form.invalid ) {
      console.log('Error de datos de formulario');
      return;
    }

    this.productoService.postProductos( this.producto )
      .subscribe( respuesta => {
        console.log(respuesta);
      });

  }
}
