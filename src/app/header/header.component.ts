import { Component } from '@angular/core';

@Component({
    selector: 'app-nav-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent {
    mensaje = 'Hola aqui hay una NAV';
    title  = 'Front Productos';
}
