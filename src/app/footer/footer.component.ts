import { Component } from '@angular/core';

@Component({
  selector: 'front-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
  autor: any = { nombre:'Mauricio', corporacion: 'Home', contacto: 'prototipo00@gmail.com' }
}
