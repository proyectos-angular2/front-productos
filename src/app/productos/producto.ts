export class Producto {

  id: number;
  productoId: number;
  productoNombre: string;
  productoCantidad: number;
  productoDescripcion: string;
  productoUbicacion: string;
  creationDate: string;

}
