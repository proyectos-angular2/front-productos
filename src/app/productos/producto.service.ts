import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Producto } from './producto';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { map, delay } from 'rxjs/operators';

@Injectable()
export class ProductoService {

  private urlProductos = 'http://localhost:9050/api/v1';
  constructor(private http: HttpClient) { }

  getProductos() {
    return this.http.get(`${ this.urlProductos }/getListaProductos`);
  }

  postProductos( producto: Producto ) {
    return this.http.post(`${ this.urlProductos }/saveProducto`, producto)
      .pipe( map( ( resp: any ) => {
          producto.productoId = resp.productoId;
          return producto;
      })
    );
  }
}
