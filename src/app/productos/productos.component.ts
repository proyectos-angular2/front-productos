import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductoService } from './producto.service';
import { Producto } from './producto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: Producto[];
  prod: Producto = new Producto();

  constructor(private productoService: ProductoService) { }

  async ngOnInit(){
    // tslint:disable-next-line: prefer-const
    let resultado: any = await this.productoService.getProductos().toPromise();
    // tslint:disable-next-line: no-string-literal
    this.productos = (resultado['Data'] as Array<any>);
  }
}
